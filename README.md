# Material Dashboard IDRD - Base Vue with Laravel 5.2

## Project setup
```
composer install
cp .env.example .env
php artisan key:generate

npm install
```

### Compiles and hot-reloads for development
```
npm run dev
php artisan serve
```

### Compiles and minifies for production
```
npm run prod
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
