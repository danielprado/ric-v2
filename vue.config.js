module.exports = {
  baseUrl: `${process.env.MIX_BASE_URL}/public`,
  pluginOptions: {
    i18n: {
      locale: 'es',
      fallbackLocale: 'es',
      localeDir: 'locales',
      enableInSFC: false
    }
  },
  configureWebpack: {
    resolve: {
      alias: {
        "@": require("path").resolve(__dirname, "/resources/js")
      }
    },
    module: {
      rules: [
        {
          test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/,
          loader: 'url-loader',
          options: {
            publicPath: process.env.NODE_ENV === 'production'
              ? '/SIM/ric-v3/public/'
              : '/public/'
          }
        }
      ]
    }
  }
}
