<?php


namespace App\Traits;


trait ApiResponse
{
    private function success( $data, $code = 200 ){
        return response()->json( $data, $code );
    }

    protected function errorResponse( $message, $code = 422 )
    {
        return response()->json([
            'message' =>  $message,
            'code'  =>  $code
        ], $code);
    }
}