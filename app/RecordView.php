<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RecordView extends Model
{
    protected $table = 'record_view';
}
