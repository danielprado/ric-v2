<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class RecordInformationRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'supercade' => 'required|numeric',
            'name'  => 'required',
            'lastname'  => 'required',
            'document'  => 'required',
            'document_type' => 'required',
            'gender'    => 'required',
            'email' => 'email',
            'phone' => 'min:7|max:10',
            'mobile'    => 'digits:10',
            'location'  => 'required',
            'stratum'   => 'required',
            'reason'    => 'required',
            'other' => 'min:3',
            'question_1'    => 'required',
            'question_2'    => 'required',
            'question_3'    => 'required',
            'observations'  => 'min:3|max:2500',
        ];
    }
}
