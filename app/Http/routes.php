<?php
session_start();
/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
//rutas con filtro de autenticación
//Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'MainController@welcome')->name('welcome');
    Route::any('/welcome', 'MainController@index');
    Route::post('login', 'MainController@login');
    Route::any('/logout', 'MainController@logout')->name('logout');
	Route::group(['middleware' => ['check.auth'] ], function () {
        Route::post('api/user/check', 'MainController@check');
        /* Routes for Vue */
        Route::group(['prefix' =>   'api'], function () {
            Route::get('user', 'MainController@auth');

            Route::resource('record-information', 'Record\RecordController', [
                'only'  =>  ['index', 'store', 'update']
            ]);
            Route::resource('counter', 'Counter\CounterController', [
                'only'  =>  ['index']
            ]);
            Route::resource('document', 'Document\DocumentController', [
                'only'  =>  ['index']
            ]);
            Route::resource('reasons', 'Reason\ReasonController', [
                'only'  =>  ['index']
            ]);
            Route::resource('genders', 'Gender\GenderController', [
                'only'  =>  ['index']
            ]);
            Route::resource('supercades', 'SuperCade\SuperCadeController', [
                'only'  =>  ['index']
            ]);
            Route::resource('location', 'Location\LocationController', [
                'only'  =>  ['index']
            ]);
            Route::post('reports', 'Report\ReportController@report');
        });
    });
//});
