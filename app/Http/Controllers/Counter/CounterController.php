<?php

namespace App\Http\Controllers\Counter;

use App\RecordView;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class CounterController extends Controller
{
    public function index()
    {
        return response()->json([
            'data'  => [
                'total' => (int) RecordView::count(),
                'month' => (int) RecordView::query()->whereBetween('created_at', [Carbon::now()->startOfMonth()->format('Y-m-d H:i:s'), Carbon::now()->endOfMonth()->format('Y-m-d H:i:s')])->count(),
                'week'  => (int) RecordView::query()->whereBetween('created_at', [Carbon::now()->startOfWeek()->format('Y-m-d H:i:s'), Carbon::now()->endOfWeek()->format('Y-m-d H:i:s')])->count(),
                'today' => (int) RecordView::query()->whereBetween('created_at', [Carbon::now()->startOfDay()->format('Y-m-d H:i:s'), Carbon::now()->endOfDay()->format('Y-m-d H:i:s')])->count(),
                'cades' => RecordView::query()->select(DB::raw('supercade_name, COUNT(*) AS count'))
                                                ->whereBetween('created_at', [Carbon::now()->startOfMonth()->format('Y-m-d H:i:s'), Carbon::now()->endOfMonth()->format('Y-m-d H:i:s')])
                                                ->groupBy('supercade')->get(),
                'status' => RecordView::query()->select(DB::raw('question_2, COUNT(*) AS count'))
                                        ->whereBetween('created_at', [Carbon::now()->startOfMonth()->format('Y-m-d H:i:s'), Carbon::now()->endOfMonth()->format('Y-m-d H:i:s')])
                                        ->groupBy('question_2')->get(),

                'users'  => RecordView::query()->select(DB::raw("UPPER( CONCAT( user_name, ' ', user_middle_name, ' ', user_lastname, ' ', user_second_lastname) ) AS user_full_name, COUNT(*) AS count"))
                    ->whereBetween('created_at', [Carbon::now()->startOfMonth()->format('Y-m-d H:i:s'), Carbon::now()->endOfMonth()->format('Y-m-d H:i:s')])
                    ->groupBy('user')->get(),

                'monthly' => RecordView::query()->select(DB::raw('MONTH(created_at) AS month, COUNT(*) AS count'))
                    ->whereBetween('created_at', [Carbon::now()->startOfYear()->format('Y-m-d H:i:s'), Carbon::now()->endOfYear()->format('Y-m-d H:i:s')])
                    ->groupBy( DB::raw('MONTH(created_at)') )->get(),

                'me'  => RecordView::query()->where( 'user', $_SESSION['Id_funcionario'] )
                                            ->whereBetween('created_at', [Carbon::now()->startOfMonth()->format('Y-m-d H:i:s'), Carbon::now()->endOfMonth()->format('Y-m-d H:i:s')])
                                            ->count(),

                'date'  => Carbon::now()->toIso8601String()
            ],
            'code'  =>  200,
        ], 200);
    }
}
