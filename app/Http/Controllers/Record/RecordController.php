<?php

namespace App\Http\Controllers\Record;

use App\Encuesta;
use App\RecordView;
use App\Transformers\RecordTransformer;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\LengthAwarePaginator;

class RecordController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $column = \request()->has( 'column' ) ? \request()->get('column') : 'id';
        $order = \request()->has( 'order' ) ? \request()->get('order') : false;
        $per_page = \request()->has( 'per_page' ) ? \request()->get('per_page') : 5;
        $query = \request()->has( 'query' ) ? \request()->get('query') : null;

        $order =  ( $order ) ? 'asc' : 'desc';

        $data = RecordView::query();

        if ( $query ) {
            $data = $data->where('id', 'LIKE', "%{$query}%")
                         ->orWhere('supercade_name', 'LIKE', "%{$query}%")
                         ->orWhere('name', 'LIKE', "%{$query}%")
                         ->orWhere('lastname', 'LIKE', "%{$query}%")
                         ->orWhere('document_type_name', 'LIKE', "%{$query}%")
                         ->orWhere('document', 'LIKE', "%{$query}%")
                         ->orWhere('gender_name', 'LIKE', "%{$query}%")
                         ->orWhere('email', 'LIKE', "%{$query}%")
                         ->orWhere('phone', 'LIKE', "%{$query}%")
                         ->orWhere('mobile', 'LIKE', "%{$query}%")
                         ->orWhere('location_name', 'LIKE', "%{$query}%")
                         ->orWhere('stratum', 'LIKE', "%{$query}%")
                         ->orWhere('reason_name', 'LIKE', "%{$query}%")
                         ->orWhere('other', 'LIKE', "%{$query}%")
                         ->orWhere('question_1', 'LIKE', "%{$query}%")
                         ->orWhere('question_2', 'LIKE', "%{$query}%")
                         ->orWhere('question_3', 'LIKE', "%{$query}%")
                         ->orWhere('observations', 'LIKE', "%{$query}%")
                         ->orWhere('created_at', 'LIKE', "%{$query}%");
        }

        $data = $data->orderBy( $column, $order )->paginate( $per_page );

        $resource = $data->getCollection()
            ->map(function($model) {
                return ( new RecordTransformer() )->transform( $model );
            })->toArray();

        $records = new LengthAwarePaginator(
            $resource,
            (int) $data->total(),
            (int) $data->perPage(),
            (int) $data->currentPage(), [
            'path' => request()->url(),
            'query' => [
                'page' => (int) $data->currentPage()
            ]
        ]);

        return response()->json([
            'data'  =>  $records,
            'code'  =>  200
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\RecordInformationRequest $request)
    {
        $form = new Encuesta;
        $form->id_supercade = $request->get('supercade');
        $form->nombres = $request->get('name');
        $form->apellidos = $request->get('lastname');
        $form->cedula = $request->get('document');
        $form->id_tipo_documento = $request->get('document_type');
        $form->genero = $request->get('gender');
        $form->mail = $request->get('email');
        $form->telefono = $request->get('phone');
        $form->celular = $request->get('mobile');
        $form->id_localidad = $request->get('location');
        $form->estrato = $request->get('stratum');
        $form->id_motivo_consulta = $request->get('reason');
        $form->otro = $request->get('other');
        $form->pregunta_1 = $request->get('question_1');
        $form->pregunta_2 = $request->get('question_2');
        $form->pregunta_3 = $request->get('question_3');
        $form->observaciones = $request->get('observations');
        $form->id_usuario = $_SESSION['Id_funcionario'];

        if ( $form->save() ) {
            return response()->json([
                'data'  =>  trans('validation.handler.success'),
                'code'  => 200
            ], 200);
        }

        return response()->json([
            'message'  =>  trans('validation.handler.unexpected_failure'),
            'code'  => 422
        ], 422);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
