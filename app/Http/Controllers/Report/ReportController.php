<?php

namespace App\Http\Controllers\Report;

use App\Attendance;
use App\ParticipantView;
use App\RecordView;
use App\Sessions;
use App\Stage;
use App\Transformers\RecordTransformer;
use App\Transformers\SuperCadeTransformer;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use Maatwebsite\Excel\Facades\Excel;
use PHPExcel_Style_Border;

class ReportController extends Controller
{
    public function report(Request $request)
    {
        $start = Carbon::parse( $request->get('start_date') )->startOfDay()->format('Y-m-d H:i:s');
        $end   = Carbon::parse( $request->get('end_date') )->endOfDay()->format('Y-m-d H:i:s');

        $data = RecordView::query()->whereBetween('created_at', [$start, $end])->get();
        $resource = new Collection($data, new RecordTransformer());
        $manager = new Manager();
        $rootScope = $manager->createData($resource);
        $data = $rootScope->toArray();

        $arr = [];

        if ( $data ) {
            foreach ( $data['data'] as $record ) {
                $arr[] = [
                    'id'    => isset( $record['id'] ) ? (int) $record['id'] : null,
                    'document_type' => isset( $record['document_type_name'] ) ? $record['document_type_name'] : null,
                    'document'  => isset( $record['document'] ) ? $record['document'] : null,
                    'supercade' => isset( $record['supercade_name'] ) ? $record['supercade_name'] : null,
                    'name'  => isset( $record['full_name'] ) ? $record['full_name'] : null,
                    'location'  => isset( $record['location_name'] ) ? $record['location_name'] : null,
                    'stratum'   => isset( $record['stratum'] ) ? $record['stratum'] : null,
                    'gender'    => isset( $record['gender_name'] ) ? $record['gender_name'] : null,
                    'email' => isset( $record['email'] ) ? $record['email'] : null,
                    'phone' => isset( $record['phone'] ) ? $record['phone'] : null,
                    'mobile'    => isset( $record['mobile'] ) ? $record['mobile'] : null,
                    'reason'    => isset( $record['reason_name'] ) ? $record['reason_name'] : null,
                    'other' => isset( $record['other'] ) ? $record['other'] : null,
                    'question_1'    => isset( $record['question_1'] ) ? $record['question_1'] : null,
                    'question_2'    => isset( $record['question_2'] ) ? $record['question_2'] : null,
                    'question_3'    => isset( $record['question_3'] ) ? $record['question_3'] : null,
                    'observations'  => isset( $record['observations'] ) ? $record['observations'] : null,
                    'created_at'    => isset( $record['created_at'] ) ? Carbon::parse( $record['created_at'] )->format('Y-m-d') : null,
                    'created_hour'  => isset( $record['created_at'] ) ? Carbon::parse( $record['created_at'] )->format('H:i:s') : null,
                    'user'  => isset( $record['user_full_name'] )  ? $record['user_full_name'] : null
                ];
            }
        }

        $data = $arr;

        Excel::load( public_path('excel/REPORTE_GENERAL.xlsx'), function ($file) use ( $data, $start, $end ) {
            $file->sheet(0, function ($sheet) use ( $data, $start, $end) {
                $row = 8;

                $sheet->cell("C3", function($cell)  {
                    $cell->setValue( isset(  $_SESSION['auth']->full_name ) ?  $_SESSION['auth']->full_name : '' );
                });
                $sheet->cell("C4", function($cell) use ( $start, $end )  {
                    $cell->setValue( "DEL $start AL $end " );
                });
                $sheet->cell("C5", function($cell)  {
                    $cell->setValue( Carbon::now()->format('Y-m-d H:i:s') );
                });

                foreach ( $data as $datum ) {
                    $sheet->row($row, $datum);
                    $row++;
                }
                $sum = $row-1;
                $styleArray = array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN
                        )
                    )
                );
                $sheet->getStyle("A8:T$sum")->applyFromArray($styleArray);
            });
        })->export('xlsx');

    }
}
