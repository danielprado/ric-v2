<?php


namespace App\Transformers;


use App\RecordView;
use League\Fractal\TransformerAbstract;

class RecordTransformer extends TransformerAbstract
{
    public function transform(RecordView $view)
    {
        return [
            'id'    =>  isset( $view->id ) ? (int) $view->id : null,
            'supercade' =>  isset( $view->supercade ) ? (int) $view->supercade : null,
            'supercade_name'    =>  isset( $view->supercade_name ) ? $view->supercade_name : null,
            'name'  =>  isset( $view->name ) ? $this->toUpper( trim( $view->name ) ) : null,
            'lastname'  =>  isset( $view->lastname ) ? $this->toUpper( trim( $view->lastname ) ) : null,
            'full_name' => isset( $view->name, $view->lastname ) ? $this->toUpper( trim( trim($view->name).' '.trim( $view->lastname ) ) ) : null,
            'document'  =>  isset( $view->document ) ? $view->document : null,
            'document_type' =>  isset( $view->document_type ) ? (int) $view->document_type : null,
            'document_type_name'    =>  isset( $view->document_type_name ) ? $view->document_type_name : null,
            'document_type_description' =>  isset( $view->document_type_description ) ? $view->document_type_description : null,
            'gender'    =>  isset( $view->gender ) ? (int) $view->gender : null,
            'gender_name'   =>  isset( $view->gender_name ) ? $view->gender_name : null,
            'email' =>  isset( $view->email ) ? $view->email : null,
            'phone' =>  isset( $view->phone ) ? $view->phone : null,
            'mobile'    =>  isset( $view->mobile ) ? $view->mobile : null,
            'location'  =>  isset( $view->location ) ? (int) $view->location : null,
            'location_name' =>  isset( $view->location_name ) ? $view->location_name : null,
            'stratum'   =>  isset( $view->stratum ) ? (int) $view->stratum : null,
            'reason'    =>  isset( $view->reason ) ? (int) $view->reason : null,
            'reason_name'   =>  isset( $view->reason_name ) ? $view->reason_name : null,
            'other' =>  isset( $view->other ) ? $view->other : null,
            'question_1'    =>  isset( $view->question_1 ) ? $view->question_1 : null,
            'question_2'    =>  isset( $view->question_2 ) ? $view->question_2 : null,
            'question_3'    =>  isset( $view->question_3 ) ? $view->question_3 : null,
            'observations'  =>  isset( $view->observations ) ? $view->observations : null,
            'created_at'    =>  isset( $view->created_at ) ? $view->created_at->format('Y-m-d H:i:s') : null,
            'user'  =>  isset( $view->user ) ? (int) $view->user : null,
            'user_name' =>  isset( $view->user_name ) ? $view->user_name : null,
            'user_middle_name'  =>  isset( $view->user_middle_name ) ? $view->user_middle_name : null,
            'user_lastname' =>  isset( $view->user_lastname ) ? $view->user_lastname : null,
            'user_second_lastname'  =>  isset( $view->user_second_lastname ) ? $view->user_second_lastname : null,
            'user_full_name'    =>  $this->toUpper( trim( trim( $view->user_name).' '. trim($view->user_middle_name).' '. trim($view->user_lastname).' '. trim($view->user_second_lastname) ) )
        ];
    }

    public function toUpper( $string = null )
    {
        return mb_convert_case( strtolower( trim( strip_tags( $string ) ) ), MB_CASE_UPPER, 'UTF-8');
    }
}