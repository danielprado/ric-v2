<?php


namespace App\Transformers;


use Idrd\Usuarios\Repo\Localidad;
use League\Fractal\TransformerAbstract;

class LocationTransformer extends TransformerAbstract
{
    public function transform(Localidad $localidad)
    {
        return [
            'id'      =>  isset( $localidad->Id_Localidad ) ?     $localidad->Id_Localidad : 0,
            'name'    =>  isset( $localidad->Nombre_Localidad ) ? $localidad->Nombre_Localidad : null
        ];
    }
}