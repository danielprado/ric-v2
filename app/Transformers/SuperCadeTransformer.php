<?php


namespace App\Transformers;


use App\SuperCade;
use League\Fractal\TransformerAbstract;

class SuperCadeTransformer extends TransformerAbstract
{
    public function transform( SuperCade $superCade )
    {
        return [
            'id'        =>  isset( $superCade->id ) ? (int) $superCade->id : null,
            'name'      =>  isset( $superCade->nombre ) ? $superCade->nombre : null
        ];
    }
}