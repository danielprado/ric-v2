<?php


namespace App\Transformers;


use App\Motivo;
use League\Fractal\TransformerAbstract;

class ReasonTransformer extends TransformerAbstract
{
    public function transform( Motivo $reason )
    {
        return [
            'id'      =>  isset( $reason->id_motivo ) ?     $reason->id_motivo : 0,
            'name'    =>  isset( $reason->motivo ) ? $reason->motivo : null
        ];
    }
}