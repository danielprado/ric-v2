<?php


namespace App\Transformers;



use App\Document;
use League\Fractal\TransformerAbstract;

class DocumentTransformer extends TransformerAbstract
{
    public function transform( Document $document)
    {
        return [
            "id"            =>  isset( $document->Id_TipoDocumento ) ? $document->Id_TipoDocumento : null,
            "name"          =>  isset( $document->Nombre_TipoDocumento,  $document->Descripcion_TipoDocumento ) ? $document->Nombre_TipoDocumento.' - '. $document->Descripcion_TipoDocumento : null,
            "description"   =>  isset( $document->Descripcion_TipoDocumento ) ? $document->Descripcion_TipoDocumento : null,
        ];
    }
}