<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SuperCade extends Model
{
    protected $table = 'supercade';
    protected $primaryKey = 'id';
    protected $fillable = ['nombre'];
    public $timestamps = false;
}
