module.exports = {
  resolve: {
    extensions: [".js", ".vue", ".json"],
    alias: {
      vue$: "vue/dist/vue.esm.js",
      "@": __dirname + "/resources/js"
    },
  },
  output: {
    chunkFilename: "js/[name].[hash:7].js",
    publicPath: process.env.NODE_ENV === 'production' ? "/SIM/ric-v2/public/" : "/public/"
  }
};
