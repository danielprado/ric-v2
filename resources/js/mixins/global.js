import {
  mapMutations,
  mapState
} from 'vuex'

export const globalMixin = {
  data: () => {
    return {
      refCount: 0,
      isLoading: false,
      unexpected: null,
    };
  },
  mounted: function() {
    window.axios.defaults.headers.common["X-Localization"] = this.$i18n.locale || 'es';
    
    this.$i18n.locale = this.lang;
    if ( this.$validator ) {
      this.$validator.localize( this.lang );
    }
    
    if ( window.isAuthenticated === false && this.$auth.auth() ) {
      this.$router.push({name: 'Lock'});
    }
  
    if ( window.isAuthenticated === false && !this.$auth.auth() ) {
      this.$router.push({name: 'Login'});
    }
  
    this.unexpected = this.$t('unexpected failure');
    this.error_file = this.$t('error_file');
  },
  methods: {
    ...mapMutations('app', ['setLang', 'setSnack', 'setSnackMessage', 'setSnackType']),
    onLang( lang = 'es') {
      this.setLang( lang );
      this.$i18n.locale = lang;
      if ( this.$validator ) {
        this.$validator.localize( lang );
      }
    },
    setLoading(isLoading) {
      if (isLoading) {
        this.refCount++;
        this.isLoading = true;
      } else if (this.refCount > 0) {
        this.refCount--;
        this.isLoading = this.refCount > 0;
      }
    },
    axiosInterceptor: function() {
      window.axios.interceptors.request.use(
        config => {
          this.setLoading(true);
          return config;
        },
        error => {
          this.setLoading(false);
          return Promise.reject(error);
        }
      );
      
      window.axios.interceptors.response.use(
        response => {
          this.setLoading(false);
          return response;
        },
        error => {
          this.setLoading(false);
          return Promise.reject(error);
        }
      );
    },
    notifyOnError( message = null ) {
      this.setSnackType( 'error' );
      this.setSnackMessage( message || this.$t('unexpected failure') );
      this.setSnack(true);
    },
    notifyOnSuccess(message) {
      this.setSnackType( 'success' );
      this.setSnackMessage( message );
      this.setSnack(true);
    },
    onCloseNotification: function () {
      this.setSnackType( 'black' );
      this.setSnackMessage( '' );
      this.setSnack(!this.$store.state.app.snack);
    }
  },
  computed: {
    ...mapState('app', ['lang', 'snack']),
  }
}