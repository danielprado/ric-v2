import Vue from 'vue'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import es from 'vuetify/es5/locale/es'
//import '@mdi/font/css/materialdesignicons.css'
import TownIcon from '@/components/icon/TownHall'
import Bogota from '@/components/icon/Bogota'

Vue.use(Vuetify, {
  theme: {
    primary: '#4caf50',
    secondary: '#4caf50',
    tertiary: '#495057',
    accent: '#82B1FF',
    error: '#f55a4e',
    info: '#00d3ee',
    success: '#5cb860',
    warning: '#ffa21a'
  },
  options: {
    customProperties: true
  },
  iconfont: 'mdi',
  icons: {
    town: {
      component: TownIcon
    },
    bogota: {
      component: Bogota
    },
  },
  lang: {
    locales: { es },
    current: 'es'
  },
})
