import { Model } from "@/models/Model";
import { Api } from "@/models/Api";

export class User extends Model {
  constructor( data ) {
    super( Api.END_POINTS.GET_PROFILE(), data );
  }

  check() {
    return this.post( Api.END_POINTS.CHECK_AUTH() );
  }

  login() {
    return this.post( Api.END_POINTS.LOGIN(), this.data() )
  }
}