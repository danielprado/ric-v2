import {Model} from "@/models/Model";
import {Api} from "@/models/Api";

export class Location extends Model {
  constructor() {
    super(Api.END_POINTS.LOCATION(), {});
  }

}