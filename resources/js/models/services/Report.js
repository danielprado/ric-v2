import {Model} from "@/models/Model";
import {Api} from "@/models/Api";

export class Report extends Model {
  constructor() {
    super( Api.END_POINTS.REPORTS(), {
      start_date: null,
      end_date: null
    });
    this.validations = {
      date: {
        required: true,
        date_format: 'yyyy-MM-dd',
        before: 'beforeTarget'
      },
      after_date: {
        required: true,
        date_format: 'yyyy-MM-dd',
        after: 'afterTarget'
      },
    }
  }

  excel( data ) {
    return window.axios({
      url: Api.END_POINTS.REPORTS(),
      method: 'POST',
      responseType: 'blob',
      data: data
    })
  }

}