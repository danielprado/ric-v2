import {Model} from "@/models/Model";
import {Api} from "@/models/Api";

export class Information extends Model {
  constructor() {
    super( Api.END_POINTS.RECORD_INFORMATION(), {
      supercade: null,
      name: null,
      lastname: null,
      document_type: null,
      document: null,
      gender: null,
      email: null,
      phone: null,
      mobile: null,
      location: null,
      stratum: null,
      reason: null,
      other: null,
      //survey
      question_1: null,
      question_2: null,
      question_3: null,
      observations: null
    })

    this.validations = {
      required: {
        required: true
      },
      numeric: {
        required: true,
        numeric: true,
      },
      phone: {
        min: 7,
        max: 10,
        numeric: true,
      },
      document: {
        required: true,
        min: 4,
      },
      mobile: {
        digits: 10,
        numeric: true,
      },
      email: {
        email: true,
        max: 80
      },
      text: {
        alpha_spaces: true,
        max: 100
      },
      text_required: {
        alpha_spaces: true,
        required: true,
        min: 2,
        max: 70
      },
      note: {
        min: 3,
        max: 2500
      },
    }
  }
}