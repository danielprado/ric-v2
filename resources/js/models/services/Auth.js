import {Model} from "@/models/Model";
import {Api} from "@/models/Api";

export class Auth extends Model {
  constructor(data) {
    super(Api.END_POINTS.LOGIN(), data);
  
    this.validations = {
      required: {
        required: true
      },
      password: {
        required: true,
        min: 5
      }
    }
    
    this.touched = {
      username: false,
      password: false
    }
  }
}