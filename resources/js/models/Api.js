let path = `${process.env.MIX_BASE_URL}/api`;
export const Api = {
  COOKIE: process.env.MIX_COOKIE_NAME,
  END_POINTS: {
    //Information
    RECORD_INFORMATION: () => { return `${path}/record-information` },
    CADES: () => { return `${path}/supercades` },
    DOCUMENT_TYPE: () => { return `${path}/document` },
    LOCATION: () => { return `${path}/location` },
    REASONS: () => { return `${path}/reasons` },
    COUNTER: () => { return `${path}/counter` },
    REPORTS: () => { return `${path}/reports` },
    // Auth
    GET_PROFILE: () => { return `${path}/user` },
    CHECK_AUTH: () => { return `${path}/user/check` },
    LOGIN: () => { return `${process.env.MIX_BASE_URL}/login` },
    LOGOUT: () => { return `${process.env.MIX_BASE_URL}/logout` },
  }
};
