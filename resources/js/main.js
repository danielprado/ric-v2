import '@babel/polyfill'
import "./bootstrap"
import Vue from 'vue'
import "./styles/animate.css"
import App from './App.vue'
import router from './router'
import store from './store'
import './registerServiceWorker'
//import 'roboto-fontface/css/roboto/roboto-fontface.css'
//import 'material-design-icons-iconfont/dist/material-design-icons.css'
import i18n from './i18n'
import { sync } from 'vuex-router-sync'
import VeeValidate, { Validator } from "vee-validate";
import es from "vee-validate/dist/locale/es";
Validator.localize("es", es);

Vue.use(VeeValidate, { fieldsBagName: "veeFields", locale: "es" })

// Components
import './components'
import './plugins'
import Auth from "./package/auth";
Vue.use(Auth);
String.prototype.capitalize = function() {
  return this.charAt(0).toUpperCase() + this.slice(1);
};

// Sync store with router
sync(store, router)

router.beforeEach((to, from, next) => {
  if ( typeof window.source == 'function') {
    window.source('Canceled by the user');
  }
  if (to.matched.some(record => record.meta.forVisitors)) {
    if (Vue.auth.isAuthenticated()) {
      next({name: 'Home'})
    } else if ( to.matched.some((record) => record.meta.can) ) {
      next();
    } else {
      next({name: 'Home'});
    }
  } else if (to.matched.some(record => record.meta.requiresAuth)) {
    if (!Vue.auth.isAuthenticated()) {
      next({name: 'Lock'});
    } else if ( to.matched.some((record) => record.meta.can) ) {
      next();
    } else {
      next({name: 'Home'})
    }
  } else {
    next();
  }
});



Vue.config.productionTip = false

import { globalMixin } from "@/mixins/global"

Vue.mixin( globalMixin )

new Vue({
  router,
  store,
  i18n,
  render: h => h(App)
}).$mount('#app')
