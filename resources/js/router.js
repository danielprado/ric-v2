import Vue from 'vue'
import Router from 'vue-router'
import DashboardLayout from "@/components/layouts/DashboardLayout"
import AuthLayout from "@/components/layouts/AuthLayout"
import ErrorLayout from "@/components/layouts/ErrorLayout"
import Auth from "@/package/auth";
Vue.use(Auth);
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: "/",
      redirect: "/login",
      component: AuthLayout,
      children: [
        {
          path: 'login',
          name: 'Login',
          component: () => import(/* webpackChunkName: "login" */ './views/auth/Login.vue'),
          meta: {
            forVisitors: true,
            can: true
          }
        },
        {
          path: 'lock',
          name: 'Lock',
          component: () => import(/* webpackChunkName: "lock" */ './views/auth/Lock.vue'),
          meta: {
            forVisitors: true,
            can: true
          }
        },
      ]
    },
    {
      path: "/",
      redirect: "/dashboard",
      name: 'Home',
      component: DashboardLayout,
      children: [
        {
          path: 'dashboard',
          name: 'Dashboard',
          component: () => import(/* webpackChunkName: "dashboard" */ './views/Dashboard.vue'),
          meta: {
            requiresAuth: true,
            can: Vue.auth.isAuthenticated(),
            hideFooter: false
          }
        },
        {
          path: 'record-information',
          name: 'Record Information',
          component: () => import(/* webpackChunkName: "information" */ './views/Record.vue'),
          meta: {
            requiresAuth: true,
            can: Vue.auth.can('register_info'),
            hideFooter: false
          }
        },
        {
          path: 'general-report',
          name: 'General Report',
          component: () => import(/* webpackChunkName: "table" */ './views/information/InformationTable.vue'),
          meta: {
            requiresAuth: true,
            can: Vue.auth.can('general_report'),
            hideFooter: false
          }
        }
      ],
    },
    /** Important, leave this route always at the end of the list to handle 404, 500 or another http errors **/
    {
      path: "/error",
      component: ErrorLayout,
      children: [
        {
          path: "",
          name: "Error",
          component: () => import(/* webpackChunkName: "error" */ './views/error/Error.vue'),
          props: true
        }
      ]
    },
    {
      path: "**",
      redirect: "/error",
    },
  ]
})
