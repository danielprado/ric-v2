/**
 * Vuex
 *
 * @library
 *
 * https://vuex.vuejs.org/en/
 */

// Lib imports
import Vue from 'vue'
import Vuex from 'vuex'
import AuthModule from "./modules/auth"
import localForage from "localforage";
import VuexPersist from "vuex-persist";

// Store functionality
import modules from './modules'

Vue.use(Vuex)

const vuexPersistEmitter = () => {
  return store => {
    store.subscribe(mutation => {
      if (mutation.type === "RESTORE_MUTATION") {
        store._vm.$root.$emit("storageReady");
      }
    });
  };
};

const debug = process.env.NODE_ENV !== "production";

localForage.config({
  name: process.env.MIX_LOCAL_DB_NAME,
  version: 1.0,
  storeName: process.env.MIX_COOKIE_NAME,
  description: "Almacena de forma offline algunos atributos de la base de datos."
});

const vuexLocalStorage = new VuexPersist({
  strictMode: true,
  asyncStorage: false,
  key: process.env.MIX_COOKIE_NAME,
  storage: localForage,
  modules: ["selects"]
});

const vuexLocalStorageSetting = new VuexPersist({
  strictMode: true,
  asyncStorage: false,
  key: process.env.MIX_COOKIE_NAME ,
  storage: window.localStorage,
  modules: ["app"]
});

const vuexSessionStorage = new VuexPersist({
  strictMode: true,
  asyncStorage: false,
  key: process.env.MIX_COOKIE_NAME,
  storage: window.sessionStorage,
  modules: ["auth"]
});

// Create a new store
const store = new Vuex.Store({
  mutations: {
    RESTORE_MUTATION: vuexLocalStorage.RESTORE_MUTATION // this mutation **MUST** be named "RESTORE_MUTATION"
  },
  modules: {
    ...modules,
    auth: AuthModule
  },
  plugins: [
    vuexPersistEmitter(),
    vuexLocalStorage.plugin,
    vuexSessionStorage.plugin,
    vuexLocalStorageSetting.plugin
  ],
  strict: debug
})

export default store
