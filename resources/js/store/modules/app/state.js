export default {
  snack: null,
  snack_message: null,
  snack_icon: 'mdi-bell-plus',
  snack_position: 'bottom',
  snack_type: 'black',
  schema: false,
  drawer: null,
  mini: false,
  drawerRight: null,
  color: 'info',
  image:  `${process.env.MIX_BASE_URL}/public/img/sidebar-2.jpg`,
  sidebarBackgroundColor: 'rgba(27, 27, 27, 0.64)',
  lang: 'es'
}
