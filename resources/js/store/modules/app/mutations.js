import { set, toggle } from '@/utils/vuex'

export default {
  setDrawer: set('drawer'),
  setSnack: set('snack'),
  setSnackMessage: set('snack_message'),
  setSnackIcon: set('snack_icon'),
  setSnackPosition: set('snack_position'),
  setSnackType: set('snack_type'),
  setDrawerRight: set('drawerRight'),
  setImage: set('image'),
  setColor: set('color'),
  setLang: set('lang'),
  setMini: set('mini'),
  setGradient: set('sidebarBackgroundColor'),
  setSchema: set('schema'),
  toggleDrawerRight: toggle('drawerRight'),
  toggleDrawer: toggle('drawer'),
  toggleMini: toggle('mini'),
  toggleSchema: toggle('drawer'),
  toggleSnackbar: toggle('snackbar')
}
