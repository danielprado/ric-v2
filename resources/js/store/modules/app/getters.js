const getters = {
  getColor: state => ( state.color ) ? state.color : null,
};

export default getters;