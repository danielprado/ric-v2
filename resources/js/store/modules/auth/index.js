import actions from '@/store/modules/auth/actions';
import getters from '@/store/modules/auth/getters';
import mutations from '@/store/modules/auth/mutations';

const state = {
    username: null,
    auth: null,
    permissions: window.permissions
};

export default { actions, mutations, getters, state }

