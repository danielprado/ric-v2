const mutations = {
    ACCENT( state, response ) {
        state.accent   = response;
    },
    BGCOLOR( state, response ) {
        state.background_color   = response;
    },
    sidebarMini( state, response ) {
        state.mini   = response;
    },
    BGIMAGE( state, response ) {
        state.background_image   = response;
    },
    LANG( state, response ) {
        state.lang   = response;
    },
    sidebarImg( state, response ) {
        state.image   = response;
    },
    RTL( state, response ) {
        state.is_RTL   = response;
    }
    
};

export default mutations;