<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>@yield('title')</title>

        <link rel="icon" href="{{ asset('public/favicon.ico') }}">
        <link rel="manifest" href="{{ asset('public/manifest.json') }}">

        <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('public/apple-touch-icon.png') }}">
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('public/favicon-32x32.png') }}">
        <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('public/favicon-16x16.png') }}">
        <link rel="manifest" href="{{ asset('public/site.webmanifest') }}">
        <link rel="mask-icon" href="{{ asset('public/safari-pinned-tab.svg') }}" color="#5bbad5">
        <meta name="apple-mobile-web-app-title" content="Registro Información - SuperCADES">
        <meta name="application-name" content="Registro Información - SuperCADES">
        <meta name="msapplication-TileColor" content="#0f1b2e">
        <meta name="msapplication-TileImage" content="{{ asset('public/mstile-144x144.png') }}">
        <meta name="theme-color" content="#0f1b2e">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        @if( isset( $_SESSION['Usuario']['Permisos'] ) )
            <meta name="user-permissions" content="{{ json_encode( $_SESSION['Usuario']['Permisos'] ) }}">
        @endif

        <script type="text/javascript">
            @if( isset( $_SESSION['Usuario']['Permisos'] ) )
              window.isAuthenticated = true;
            @else
              window.isAuthenticated = false;
            @endif
        </script>


        <!-- Fonts -->
        <link rel="dns-prefetch" href="//fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .code {
                border-right: 2px solid;
                font-size: 26px;
                padding: 0 15px 0 15px;
                text-align: center;
            }

            .message {
                font-size: 18px;
                text-align: center;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="code">
                @yield('code')
            </div>

            <div class="message" style="padding: 10px;">
                @yield('message')
            </div>
        </div>
    </body>
</html>
