
<!DOCTYPE html>

<html lang="es">
<head>
<style type="text/css">
	
	@page { margin: 0px; }
	body { margin: 0px;}

	#name{
		  text-transform: uppercase;
		  position: relative;
		  top: 128px;
		  left: 23px;
		  font: bold ;
		  font-family: "Tahoma", "Geneva", sans-serif;
		  font-size: 80%;
		  font-style: oblique;
		  
	}

	#document{
		  text-transform: uppercase;
		  position: relative;
		  top: 115px;
		  left: 23px;
		  font: bold ;
		  font-family: "Tahoma", "Geneva", sans-serif;
		  font-size: 80%;
		  font-style: oblique;
		  
	}
	
</style>
 <meta charset="UTF-8">
 <title>Pasaporte {{ isset( $request['passport'] ) ? $request['passport'] : 0 }}</title>
	
</head>
<body >


 	@if($request)
		<p id="name">{{$request['name']}} </p>
		<p id="document">{{ $request['document_type']  }}. {{$request['document']}}   &nbsp;&nbsp;     -      &nbsp;&nbsp; N°. {{$request['passport']}}</p>
	@endif

</body>
</html>